document.getElementById('navigation-box').innerHTML = `
<ul class="nav flex-column">
    <li class="nav-item">
        <a class="nav-link active" href="/">Main</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="/process-suspender.html">> Process suspender</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="/auto-clicker.html">> Auto clicker</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="/cruise-control.html">> Cruise control</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="/afk.html">> AFK mode</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="/about.html">> About</a>
    </li>
</ul>
`;
