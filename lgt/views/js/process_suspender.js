var activate_status = true;
var suspend_time = 0;
var suspend_name = '';

function process_suspender() {
    get_usr_input();

    // check if activation available
    if (activate_status === true) {
        // send process and time
        server_command('process-suspender-time-' + suspend_time);
        console.log('process-suspender-time-' + suspend_time);
        server_command('process-suspender-process-' + suspend_name);
        console.log('process-suspender-process-' + suspend_name);
    } else {
        console.log('Activation status: ', activate_status);
    }
}

function get_usr_input() {
    activate_status = true;
    // get time
    let time = document.getElementById('time_field').value;
    // get process
    let process_name = document.getElementById('text_field').value;

    // check input time
    if (time === '') {
        document.getElementById('time_filed_message').innerHTML =
            "<span style='font-size: 0.9em;'>No input detected ></span><span style='color: yellowgreen; font-size: 0.9em;'> time was set to 10</span>";
        document.getElementById('time_field').value = 10;
        suspend_time = 10;
    } else if (time < 0) {
        document.getElementById('time_filed_message').innerHTML =
            "<span style='font-size: 0.9em;'>No input detected</span><span style='font-size: 0.9em;'> > time was set to 10</span>";
        document.getElementById('time_field').value = 10;
        suspend_time = 10;
    } else {
        // if number is valid convert str to int
        suspend_time = time;

        // reset time field error message
        document.getElementById('time_filed_message').innerHTML = '<span></span>';
    }

    // check process input
    if (process_name === '') {
        // display error message
        document.getElementById('text_field_message').innerHTML =
            "<span style='color: red; font-size: 0.9em;'>Can't be empty</span>";

        // disable activation
        activate_status = false;
    } else {
        // set process name
        suspend_name = process_name;

        // reset text field error message
        document.getElementById('text_field_message').innerHTML = '<span></span>';
    }
}

// ajax server command
function server_command(command) {
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET', command);
    xhttp.send();
}
