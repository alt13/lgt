@ECHO OFF
tasklist /FI "IMAGENAME eq notepad.exe" 2>NUL | find /I /N "notepad.exe">NUL
if "%ERRORLEVEL%"=="0" (
start pssuspend64.exe notepad.exe
echo WORKING...
timeout /t 10 /nobreak
start pssuspend64.exe -r notepad.exe
) else (
Echo notepad.exe is not running
pause
)