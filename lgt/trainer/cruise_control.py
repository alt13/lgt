import keyboard
from keys import ReleaseKey, PressKey, W

def activate_cruise_control():
	PressKey(W)

def deactivate_cruise_control():
	ReleaseKey(W)
