import keyboard
from keys import ReleaseKey, PressKey, W, S
import time
from threading import Thread

status = True
thread_on = False

def activate_afk():
    global status
    status = True
    global thread_on
    if (thread_on == False):
        Thread(target = afk_on).start()
    else:
        pass


def afk_on():
    while True:
        global status
        if  (status == True):
            PressKey(W)
            PressKey(S)
            time.sleep(0.2) # typical keypress duration 50mS-300mS ish
            ReleaseKey(W)
            ReleaseKey(S)
            # keyboard.press_and_release('w, s')
            # print('key press')
            time.sleep(10)
        else:
            break


def deactivate_afk():
    global status
    status = False



