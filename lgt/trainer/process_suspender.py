import subprocess

timer_time = 0
process_name = ''

def process_suspender(path):
    read_request(path)
    

def read_request(path):
    time_path = path[1:23]
    process_path = path[1:26]
    global timer_time
    global process_name

    try:
        # get time from request
        if (time_path == 'process-suspender-time'):
            timer_time = int(path[path.find('process-suspender-time')+23:])
        # get name from requests
        elif (process_path == 'process-suspender-process'):
            process_name = path[path.find('process-suspender-process')+26:]

            # activate
            activate_suspender()
    except Exception as e:
        print("An error occurred \n", e)


def create_suspender_file():
    global timer_time
    global process_name
    try:
            # create/overwrite bat file
            f = open("suspender.bat", "w")
            f.write('@ECHO OFF' + '\n')
            f.write('tasklist /FI "IMAGENAME eq ' + process_name + '" 2>NUL | find /I /N "' + process_name + '">NUL' + '\n')
            f.write('if "%ERRORLEVEL%"=="0" ('  + '\n')
            f.write('start pssuspend64.exe ' + process_name + '\n')
            f.write('echo WORKING...' + '\n')
            f.write('timeout /t ' + str(timer_time) + ' /nobreak' + '\n')
            f.write('start pssuspend64.exe -r ' + process_name + '\n')
            f.write(') else (' + '\n')
            f.write('Echo ' + process_name +' is not running' + '\n')
            f.write('pause' + '\n')
            f.write(')')
            f.close()
    except Exception as e:
        print("An error occurred \n", e)


def activate_suspender():
    create_suspender_file()
    # start
    try:
        subprocess.call([r'suspender.bat'])
    except Exception as e:
        print("An error occurred \n", e)
    