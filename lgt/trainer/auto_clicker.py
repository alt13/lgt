import keyboard
from keys import ReleaseKey, PressKey
import mouse
import time
from threading import Thread

status = True
thread_on = False
timer_time = 0
key_list = []
mouse_list = []
mode = ''

# initial function
def auto_clicker(path):
    read_request(path)
    

def read_request(path):
    activate_path = path[1:22]
    deactivate_path = path[1:24]
    clicker_time = path[1:22]

    try:
        # get time from URL
        if (clicker_time == 'auto-clicker-set-time'):
            set_time(int(path[path.find("auto-clicker-activate")+24:]))

        # get activate requests
        elif (activate_path == 'auto-clicker-activate'):
            # print('[DEBUG] activate detected: ', activate_path)

            # split sting and add to list
            keys = path[path.find("auto-clicker-activate")+22:]
            global key_list
            key_list = keys.split(',')
            # print('INITIAL LIST: ', key_list)

            # check for mouse keys (if yes remove from key_list and add to the mouse list)
            global mouse_list
            if 'mouse_left' in key_list:
                key_list.remove("mouse_left")
                mouse_list.append("left")
            if 'mouse_right' in key_list:
                key_list.remove("mouse_right")
                mouse_list.append("right")
            if 'mouse_middle' in key_list:
                key_list.remove("mouse_middle")
                mouse_list.append("middle")

            activate_clicker()

        # get deactivate requests
        elif (deactivate_path == 'auto-clicker-deactivate'):
            # print('[DEBUG] deactivate detected')
            deactivate_clicker()
    except Exception as e:
        print("An error occurred \n", e)


def set_time(time):
    global timer_time
    timer_time = time


def check_if_mouse_list_empty():
    status = True
    if len(mouse_list) != 0:
        status = False
    return status


def check_if_key_list_empty():
    status = True
    if len(key_list) != 0:
        status = False
    return status


def mouse_key_press():
    for i in range(len(mouse_list)):
        mouse.press(button=mouse_list[i])


def keyboard_key_press():
    # changing type to hex and press key
    for i in range(len(key_list)):
        hex_string = key_list[i]
        an_integer = int(hex_string, 0)
        PressKey(an_integer)


def mouse_key_release():
    for i in range(len(mouse_list)):
        mouse.release(button=mouse_list[i])


def keyboard_key_release():
    for i in range(len(key_list)):
        hex_string = key_list[i]
        an_integer = int(hex_string, 0)
        ReleaseKey(an_integer)


def reset():
    global timer_time
    timer_time = 0
    global key_list
    key_list = []
    global mouse_list
    mouse_list = []
    


def activate_clicker():
    global status
    status = True
    global thread_on
    
    # set mode
    global mode
    # mouse only
    if (check_if_mouse_list_empty() == False and check_if_key_list_empty() == True):
        mode = 'mouse'
    # keyboard only
    elif (check_if_mouse_list_empty() == True and check_if_key_list_empty() == False):
        mode = 'keyboard'
    # mouse and keyboard
    elif (check_if_mouse_list_empty() == False and check_if_key_list_empty() == False):
        mode = 'mouse_and_keyboard'
    else:
        pass

    # turn on clicker only if thread doesn't exist
    if (thread_on == False):
        # start thread for specific mode
        if(mode == 'mouse'):
            Thread(target = clicker_loop_mouse).start()
        elif(mode == 'keyboard'):
            Thread(target = clicker_loop_keyboard).start()
        elif(mode == 'mouse_and_keyboard'):
            Thread(target = clicker_loop_mouse_and_keyboard).start()
    

def clicker_loop_mouse():
    global timer_time
    global status
    mouse_hold = False
    
    while True:
        if  (status == True):
            if (timer_time > 0):
                mouse_key_press()
                mouse_key_release()
                time.sleep(timer_time)

            # don't release if timer  = 0 (hold)
            elif(timer_time == 0):
                if (mouse_hold == False):
                    mouse_key_press()
                    mouse_hold = True
        else:
            break


def clicker_loop_keyboard():
    global timer_time
    global status
    keyboard_hold = False
    
    while True:
        if  (status == True):
            if (timer_time > 0):
                keyboard_key_press()
                keyboard_key_release()
                time.sleep(timer_time)

            # don't release if timer  = 0 (hold)
            elif(timer_time == 0):
                if (keyboard_hold == False):
                    keyboard_key_press()
                    keyboard_hold = True
        else:
            break


def clicker_loop_mouse_and_keyboard():
    global timer_time
    global status
    hold = False

    while True:
        if (status == True):
            if (timer_time > 0):
                mouse_key_press()
                keyboard_key_press()
                mouse_key_release()
                keyboard_key_release()
                time.sleep(timer_time)

            # don't release if timer  = 0 (hold)
            elif(timer_time == 0):
                if (hold == False):
                    mouse_key_press()
                    keyboard_key_press()
                    hold = True
        else:
            break



def deactivate_clicker():
    # break loop thread
    global status
    status = False

    # deactivate keys if timer_time = 0
    global timer_time
    global mode
    if(timer_time == 0):
        if(mode == 'mouse'):
            mouse_key_release()
        elif(mode == 'keyboard'):
            keyboard_key_release()
        elif(mode == 'mouse_and_keyboard'):
            mouse_key_release()
            keyboard_key_release()

    # reset variables
    reset()