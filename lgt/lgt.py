import socket

# trainer import
from trainer.cruise_control import activate_cruise_control, deactivate_cruise_control
from trainer.afk_mode import activate_afk, deactivate_afk
from trainer.auto_clicker import auto_clicker
from trainer.process_suspender import process_suspender

debug_mode = True


def start_server():
    # creating socket
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #server = socket.create_server(('127.0.0.1'), 2000) # the same

    # get host ip
    host_ip=(socket.gethostbyname(socket.gethostname()))

    print(' Open control panel in you browser:\n', host_ip + ':3000\n')

    # bindind IP and port to socket (socket without address - client socket)
    server.bind((host_ip, 3000))

    # listen port
    server.listen(4) # 4 - max number of incoming requests
    try:
        while True:
            if (debug_mode == True):
                print(' Waiting...')
            else:
                pass

            # Receive requests
            client_socket, address = server.accept()
            data = client_socket.recv(1024).decode('utf-8') # 1024 - size in 
            # print(data)

            # Response 
            content = request_handler(data)
            client_socket.send(content)

            # close connection
            client_socket.shutdown(socket.SHUT_WR)
    except KeyboardInterrupt:
        server.close()
    print(' Shuting down this piece of shit...')

def request_handler(request_data):
    HDRS = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=utf-8\r\n\r\n'
    HDRS_404 = 'HTTP/1.1 404 OK\r\nContent-Type: text/html; charset=utf-8\r\n\r\n'
    path = request_data.split(' ')[1]   # getting this line 'GET /request HTTP/1.1'
    response = ''

    server_commands = {
        # cruise control
        '/activate-cruise-control' : activate_cruise_control,
        '/deactivate-cruise-control' : deactivate_cruise_control,
        # afk mode
        '/activate-afk' : activate_afk,
        '/deactivate-afk' : deactivate_afk,
        }
    if (debug_mode == True):
        print(' >>> REQUEST:  ', path)
    else:
        pass

    try:
        # REGISTER CLIENT COMMANDS
        if path in server_commands:
            if (debug_mode == True):
                print('     SERVER:   ', path, 'registered')
            else:
                pass

            # call function by url
            server_commands[path]()

            # send empty response
            with open('views/empty', 'rb') as file:
                if (debug_mode == True):
                    print(' <<< RESPONSE:  empty \n')
                else:
                    pass

                response = file.read()
            return HDRS.encode('utf-8') + response


        # catch process suspender commands
        elif (path[path.find("process-suspender-"):path.find("process-suspender-")+18] == 'process-suspender-'):
            process_suspender(path)
            if (debug_mode == True):
                print('     SERVER:   ', path, 'registered')
            else:
                pass
            # send empty response
            with open('views/empty', 'rb') as file:
                if (debug_mode == True):
                    print(' <<< RESPONSE:  empty \n')
                else:
                    pass
                response = file.read()
            return HDRS.encode('utf-8') + response


        # catch auto clicker commands
        elif (path[path.find("auto-clicker-"):path.find("auto-clicker-")+12] == 'auto-clicker'): # substract 'auto-clicker' from request
            auto_clicker(path)
            if (debug_mode == True):
                print('     SERVER:   ', path, 'registered')
            else:
                pass
            # send empty response
            with open('views/empty', 'rb') as file:
                if (debug_mode == True):
                    print(' <<< RESPONSE:  empty \n')
                else:
                    pass
                response = file.read()
            return HDRS.encode('utf-8') + response

        # REDIRECTS
        elif (path == '/'):
            with open('views/index.html', 'rb') as file:    # use '\' for windows
                if (debug_mode == True):
                    print(' <<< RESPONSE: ', path, '\n')
                else:
                    pass

                response = file.read()
            return HDRS.encode('utf-8') + response

        # PAGE REQUESTS
        else:
            with open('views' + path, 'rb') as file:
                if (debug_mode == True):
                    print(' <<< RESPONSE: ', path, '\n')
                else:
                    pass

                response = file.read()
            return HDRS.encode('utf-8') + response

    except FileNotFoundError:
        with open('views/404.html', 'rb') as file:
            if (debug_mode == True):
                print(' <<< RESPONSE:  /404.html', '\n')
            else:
                pass

            response = file.read()
        return HDRS_404.encode('utf-8') + response



print(
'''
     __    ____________
    / /   / ____/_  __/
   / /   / / __  / /   
  / /___/ /_/ / / /    
 /_____/\____/ /_/     
'''
    )
if __name__ == '__main__':
    start_server()
