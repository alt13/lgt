function process_suspender_indicator() {
    // get time from input
    let field_time = document.getElementById('time_field').value;
    field_time = parseInt(field_time);
    let waiting_time = (field_time + 3) * 1000;

    // get text from input
    let text = document.getElementById('text_field').value;

    if (text !== '') {
        // switch status to on
        document.getElementById('status').innerHTML =
            '<span id="status" style="color: yellowgreen;">ON</span>';

        // wait
        setTimeout(() => {
            status_off();
        }, waiting_time);
    }
}

function status_off() {
    // switch status to off
    document.getElementById('status').innerHTML =
        '<span id="status" style="color: darkorange;">OFF</span>';
}
