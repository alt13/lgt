var key_dict = {};
var btn_click_list;
var activate_status = true;

function register_key(key_name) {
    // check if key in the dictionary
    if (key_name in key_dict) {
        console.log('KEY WAS FOUND');
        if (key_dict[key_name] === true) {
            key_dict[key_name] = false;
            console.log(key_name, key_dict[key_name]);
            //change color to grey
            document.getElementById(key_name).className = 'btn btn-outline-secondary';
        } else if (key_dict[key_name] === false) {
            key_dict[key_name] = true;
            console.log(key_name, key_dict[key_name]);
            //change color to green
            document.getElementById(key_name).className = 'btn btn-outline-success';
        }
    } else {
        console.log('KEY WAS NOT FOUND \nadding ', key_name, ' to the dictionary ...');
        // adding key to the dictionary
        key_dict[key_name] = true;
        console.log(key_name, key_dict[key_name]);

        //change button color to green
        document.getElementById(key_name).className = 'btn btn-outline-success';
    }
}

function getKeyByValue(object, value) {
    return Object.keys(object).filter((key) => object[key] === value);
}

// ajax server command
function server_command(command) {
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET', command);
    xhttp.send();
}

function activate_auto_clicker() {
    // get time from input
    let time = document.getElementById('time_field').value;

    // check input time
    if (time === '') {
        document.getElementById('message').innerHTML =
            "<span style='font-size: 0.9em;'>No input detected > </span><span style='color: yellowgreen; font-size: 0.9em;'>time was set to 0</span>";
        document.getElementById('time_field').value = 0;
        time = 0;
    } else if (time < 0) {
        document.getElementById('message').innerHTML =
            "<span style='font-size: 0.9em;'>No input detected > </span><span style='color: yellowgreen; font-size: 0.9em;'>time was set to 0</span>";
        document.getElementById('time_field').value = 0;
        time = 0;
    }

    // get activated key from the list
    btn_click_list = getKeyByValue(key_dict, true);
    // console.log(typeof btn_click_list, btn_click_list);

    // check if activation available
    if (activate_status === true) {
        console.log('activation status: ', activate_status);
        // send time
        clicking_time = time;
        console.log('clicking_time: ', clicking_time);
        console.log('auto-clicker-set-time-' + time);
        server_command('auto-clicker-set-time-' + time);

        // get activated key from the list
        btn_click_list = getKeyByValue(key_dict, true);

        // create key list
        let key_array = [];
        for (let i = 0; i < btn_click_list.length; i++) {
            key_array.push(btn_click_list[i]);
        }
        // send keys list
        console.log(key_array);
        server_command('auto-clicker-activate-' + key_array);

        // disable activation
        activate_status = false;
        console.log('activation status: ', activate_status);
    } else {
        console.log('Restarting');
        restart_auto_clicker();
    }
}

function deactivate_auto_clicker() {
    // remove input message
    document.getElementById('message').innerHTML = '';

    // send request
    server_command('auto-clicker-deactivate');

    // enable activation
    activate_status = true;
}

function restart_auto_clicker() {
    console.log('Deactivating auto clicker');
    deactivate_auto_clicker();
    console.log('Activating auto clicker');
    activate_auto_clicker();
}
