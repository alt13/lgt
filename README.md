# Legal Gaming Trainer

**LGT** (Legal Gaming Trainer) - universal open source gaming trainer controlled from the browser. It's absolutely safe to use program with no risk to get banned (no game vulnerabilities have been used). Specially designed for gamers to make their lives easier.

# Files

[Download](https://gitlab.com/alt13/lgt/-/tree/master/compiled_v1.0) from compiled folder.

## Screenshots

![](https://gitlab.com/alt13/lgt/-/raw/master/screenshots/1.png)
![](https://gitlab.com/alt13/lgt/-/raw/master/screenshots/2.png)
